﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.IO;

namespace RSAManaged.KeyGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private RSACryptoServiceProvider _rsa = null;

        public MainWindow()
        {
            InitializeComponent();
            this.GenerateKey();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (this._rsa != null)
            {
                this._rsa.Clear();
            }

            base.OnClosed(e);
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            this.GenerateKey();
        }

        private void GenerateKey()
        {
            try
            {
                this.txtExponent.Text = null;
                this.txtMosulus.Text = null;
                this.txtD.Text = null;
                this.btnSave.IsEnabled = false;
                if (this._rsa != null)
                {
                    this._rsa.Clear();
                }


                this._rsa = new RSACryptoServiceProvider();
                RSAParameters parameters = this._rsa.ExportParameters(true);

                this.txtExponent.Text = BitConverter.ToString(parameters.Exponent);
                this.txtMosulus.Text = BitConverter.ToString(parameters.Modulus);
                this.txtD.Text = BitConverter.ToString(parameters.D);

                this.btnSave.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.SaveFileDialog sfd = new Microsoft.Win32.SaveFileDialog();
                sfd.Filter = "RSA key file|*.*";
                sfd.FileName = "RSA";

                if (sfd.ShowDialog() == true)
                {
                    string publicKey = this._rsa.ToXmlString(false);
                    string privateKey = this._rsa.ToXmlString(true);

                    File.WriteAllText(sfd.FileName + ".PublicKey.xml", publicKey);
                    File.WriteAllText(sfd.FileName + ".PrivateKey.xml", privateKey);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
