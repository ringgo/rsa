﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Reflection;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace RSAManaged.SilverlightTestApp
{
    public partial class MainPage : UserControl
    {
        RSAPublicKey _publicKey = null;
        RSAPrivateKey _privateKey = null;

        public MainPage()
        {
            InitializeComponent();

            string privateKeyXmlString = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("RSAManaged.SilverlightTestApp.RSA.PrivateKey.xml")).ReadToEnd();
            string publicKeyXmlString = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("RSAManaged.SilverlightTestApp.RSA.PublicKey.xml")).ReadToEnd();
            this._privateKey = RSAPrivateKey.FromXmlString(privateKeyXmlString);
            this._publicKey = RSAPublicKey.FromXmlString(publicKeyXmlString);
        }

        private void btnPrivateKeyEncrypt_Click(object sender, RoutedEventArgs e)
        {
            string input = this.txtInput.Text;
            byte[] inputData = Encoding.UTF8.GetBytes(input);
            byte[] inputDateEnc = RSAManaged.Encrypt(inputData, this._privateKey);
            this.txtEncrpty.Text = Convert.ToBase64String(inputDateEnc);

            byte[] inputDataDec = RSAManaged.Decrypt(inputDateEnc, this._publicKey);
            string inputDec = Encoding.UTF8.GetString(inputDataDec, 0, inputDataDec.Length);
            MessageBox.Show(string.Format("Public Key Decrypt Result:{0}", inputDec));
        }

        private void btnPublicKeyEncrypt_Click(object sender, RoutedEventArgs e)
        {
            //加密
            string input = this.txtInput.Text;
            byte[] inputData = Encoding.UTF8.GetBytes(input);
            byte[] inputDateEnc = RSAManaged.Encrypt(inputData, _publicKey);
            this.txtEncrpty.Text = Convert.ToBase64String(inputDateEnc);
            
            
            //解密
            //byte[] inputDataDec = RSAManaged.Decrypt(Convert.FromBase64String(result), _privateKey);
            //string inputDec = Encoding.UTF8.GetString(inputDataDec, 0, inputDataDec.Length);

            //MessageBox.Show(string.Format("Private Key Decrypt Result:{0}", inputDec));
        }

      
    }
}
